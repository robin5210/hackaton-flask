import logging
import signal as sg
import sys
import uuid
import sqlite3

from datetime import datetime

import requests
from blinker import signal
from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO

import rfid_reader
import RPi.GPIO as GPIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, logger=logging)
CORS(app)
logging.basicConfig(filename='scan.log', format='%(asctime)s - %(message)s', level=logging.DEBUG)
conn = sqlite3.connect('data.db')

id_to_name = {
    '17119068225': 'Robin',
}

received_signal = signal('scanned')

@app.route('/demo')
@app.route('/demo/<name>')
def demo(name=None):
    if name is None:
        name = 'Demo Fietser'
    received_signal.send('Demo test scan')
    return 'changed'


def emit_scan(data):
    socketio.emit('scan', {'code': id_to_name.get(data, 'Onbekend'), 'name': id_to_name.get(data, 'Onbekend')})
    payload = {
        'code': data,
        'uuid': uuid.uuid4(),
        'timestamp': datetime.utcnow().isoformat()
    }

    conn.execute('INSERT INTO scan_log (uuid, time, person) VALUES (%s, %s, %s)',
                 (payload['uuid'], payload['timestamp'], 1))
    try:
        r = requests.post("https://httpbin.org/post", data=payload, timeout=2)
        if r.status_code != 200:
            logging.error('Couldn\'t reach api for scan')
        print(r.text)
    except requests.exceptions.Timeout:
        print('Request timeout')

received_signal.connect(emit_scan)

def end_read(signal, frame):
    GPIO.cleanup()
    sys.exit()

sg.signal(sg.SIGINT, end_read)

if __name__ == '__main__':
    reader = rfid_reader.RFIDReader()
    reader.start()
    socketio.run(app, host='0.0.0.0', log_output=True)
