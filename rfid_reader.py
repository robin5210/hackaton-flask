import logging
import threading

from app import received_signal
from pirc522 import RFID


class RFIDReader(threading.Thread):

    running = True
    rdr = RFID()
    util = rdr.util()
    util.debug = True
    previous_scan = None

    def run(self):
        while self.running:
            self.rdr.wait_for_tag()
            (error, data) = self.rdr.request()
            (error, uid) = self.rdr.anticoll()
            if not error:
                scan = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
                if scan != self.previous_scan:
                    self.previous_scan = scan
                    logging.info('Scanned: {}'.format(scan))
                    received_signal.send(scan)
                    print('Card read UID: {}'.format(scan))
