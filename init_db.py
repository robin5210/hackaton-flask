import sqlite3

conn = sqlite3.connect('data.db')

conn.execute('''CREATE TABLE scan_log
             (id integer AUTOINCREMENT, person integer, uuid text, time text)''')
conn.execute('''CREATE TABLE persons
             (id integer AUTOINCREMENT, name text, distance real, month_distance real, total_distance real, last_insert text)''')
